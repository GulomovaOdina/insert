-- Add a new film to the film table
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, 
                  length, replacement_cost, rating, special_features)
VALUES ('Inception', 
        'A thief who enters the dreams of others to steal their secrets from their subconscious.', 
        2010, 1, 7, 4.99, 148, 19.99, 'PG-13', '{Behind the Scenes}')
ON CONFLICT DO NOTHING; -- This prevents inserting duplicate films if the title already exists

-- Add actors to the actor table
INSERT INTO actor (first_name, last_name)
VALUES ('Leonardo', 'DiCaprio'), ('Joseph', 'Gordon-Levitt'), ('Ellen', 'Page')
ON CONFLICT DO NOTHING; -- This prevents inserting duplicate actors if they already exist

-- Add actors to the film_actor table
INSERT INTO film_actor (actor_id, film_id)
VALUES (1, 3), (2, 3), (3, 3)
ON CONFLICT DO NOTHING; -- This prevents inserting duplicate entries

-- Add the new film to store inventory
INSERT INTO inventory (film_id, store_id)
VALUES ((SELECT film_id FROM film WHERE title = 'Inception'), 1), -- Replace 1 with the actual store_id where you want to add the film
       ((SELECT film_id FROM film WHERE title = 'Inception'), 2)
ON CONFLICT DO NOTHING; -- This prevents inserting duplicate inventory entries
